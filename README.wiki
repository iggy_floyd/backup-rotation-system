== Abstract ==

''' @2014 I. Marfin '''   ''<Unister Gmbh>''


This is a backup-rotation framework written in Shell/C++. To have it properly running, you need to 
specify a valid dump directory

         cayley.bktree.dump.dirname = /home/bla/bla/bla/

a valid backup directory 

         cayley.bktree.backup.dirname = /home/bla/bla/bla/

and a valid crash directory 

         cayley.bktree.crashed.dirname = /home/bla/bla/bla

in the setup/config.ini file.

Also you have to fix 

         #define LOCKWRITEFILE

and 

         #define LOCKREADFILE

in the test/lockfile.cpp to the proper path of the dump directory.


==  Brief HOW-TO  ==


* To start using the package in your project, you have to prepare it following the steps:



         1) check that you have installed components needed

                make
     
         2)  go to the project place, then initialize the backup-system

                cd setup; source setup_cayley_bktree.sh;  ./daemon_backup_rotation.sh start; sleep 6;
            
         
* To clean the project ( the '''cleaning''' can be used in Automake or Makefile ), please do

              make clean

        2) open a new terminal, and go to the project place, then start the backup-system

              echo "BACKUP_STOP" | nc -q1 localhost 9770


* To test the project (will automatically start the backup-system)

              make test



== Contact ==

Igor Marfin ''<Igor.Marfin@unister-gmbh.de>''

