# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project




all: configuration doc README.wiki build test configure 


# to check that the system has all needed components
configuration: configure
	@./configure
	@ cd cxxtools; ./configure; make


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.wiki
	-@echo "\n\nDoc building...\n\n"
	-@ mkdir doc
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc; rm -r {}" | sh


test: start_daemon run
	-@echo "\n\nTesting... done\n\n"

start_daemon:
	-@echo "\n\nStarting daemon...\n\n"
#	-@cd setup; source setup_cayley_bktree.sh; ./daemon_backup_rotation.sh start; sleep 6



build:
	-@echo "\n\nBuilding...\n\n"
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @compile@ {} | sed 's/@compile@//' | sh " | sh;  cd - 2> /dev/null; done

link:
	-@echo "\n\nLinking...\n\n"
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @link@ {} | sed 's/@link@//' | sh " | sh;  cd - 2> /dev/null; done	
 


run:  build link
	-@echo "\n\nRunning...\n\n"
	-@for dir in `find ./  -maxdepth 1 -type d | grep -v cxxtools | awk '{if(NR>1) print $0}'`; do echo $$dir; cd $$dir; find ./ -iname "*.cpp" | xargs -I {}  echo "grep @run@ {} | sed 's/@run@//' | sh " | sh;  cd - 2> /dev/null; done		


# to clean all temporary stuff
clean: 
	-@echo "\n\nCleaning...\n\n"
	-@cd cxxtools;  make clean;
	-rm test/*.o
	-rm -r test/.libs
	-find test/ -executable -type f | grep -v test.py | xargs -I {} rm -r {}
	-@cd setup; source setup_cayley_bktree.sh; ./daemon_backup_rotation.sh stop


.PHONY: configuration clean all doc run test build start_daemon link
