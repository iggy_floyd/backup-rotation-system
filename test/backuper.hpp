/****h* BK-TREE/backuper
  *  NAME
  *    bktree   realization of the communicator with a backup system
  *
  *  COPYRIGHT
  *    "(c) 2014 by <Unister Gmbh/Igor Marfin>"
  *
  *  SYNOPSIS
  *
  *
  *
  *  DESCRIPTION
  *    * definition of the class
  *
  *  INPUTS
  *
  *  RESULT
  *
  *  EXAMPLE
  *
  *  NOTES
  *
  *  BUGS
  *  SEE ALSO
  *   BK-TREE/bktree/tools/logger
  *
  ******
  * You can use this space for remarks that should not be included
  * in the documentation.
  */

#ifndef _BACKUPER_HPP_
#define _BACKUPER_HPP_


/****** BK-TREE/backuper/definition
 * DESCRIPTION
 *    definition of the communicator with a backup service
 * SOURCE
*/


#include <exception>
#include <iostream>
#include <cxxtools/net/tcpstream.h>
#include <sstream>      // std::stringstream, std::stringbuf
#include <unistd.h>
#include <fstream>
#include <cstdio>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>


namespace tools {


    namespace backup {


        namespace helper {


            typedef boost::shared_mutex Lock;
            typedef boost::unique_lock< Lock > WriteLock;
            typedef boost::shared_lock< Lock > ReadLock;


            class LockFile {
                public:
                    LockFile(const char * lockfilename,bool autoclean=true):_lockfilename(lockfilename),_autoclean(autoclean),_created(false) {}
                    ~LockFile() {
                        if (_autoclean)  {
                            unlock();
                        } 

                    }
                    void lock() {
                        while (isExist()) {} //check for deadlocks here
                        WriteLock lock(LockFile::mutex);
                        try {
                            std::fstream _file;
                            _file.open(_lockfilename.c_str(),std::ios::out);
                            _file<<std::flush;
                            _file.close();
                            _created=true;
                        } catch (std::exception & e) {
                          lock.unlock();
                        }
                        lock.unlock();
                    }

                    bool try_lock() {
                        if (isExist()) {
                            return false;
                        }
                        else {
                            WriteLock lock(LockFile::mutex);
                            try {
                                std::fstream _file;
                                _file.open(_lockfilename.c_str(),std::ios::out);
                                _file<<std::flush;
                                _file.close();
                                _created=true;
                            } catch (std::exception & e) {
                               lock.unlock();
                            }
                            lock.unlock();
                            return true;
                        }
                        return false;
                    }

                    void unlock(bool external=true) {
                        if (isExist()) {
                             WriteLock lock(LockFile::mutex);
                             try {
                                if (_created) {
                                    std::cout<<_lockfilename<<": "<<"Removing own lock"<<std::endl;
                                    std::remove(_lockfilename.c_str());
                                }
                                else {
                                    std::cout<<_lockfilename<<": "<<"Removing non-own lock"<<std::endl;
                                    if (external) {
                                        std::cout<<_lockfilename<<": "<<"Removing non-own lock--> done"<<std::endl;
                                        std::remove(_lockfilename.c_str());
                                    }
                                }
                             } catch (std::exception & e) {
                                lock.unlock();
                             }
                             _created=false;
                             lock.unlock();
                        }
                    }

                private:
                    bool isExist() {
                        ReadLock lock(LockFile::mutex);
                        bool isOK = false;
                        try {
                            std::fstream lockile(_lockfilename.c_str());
                            isOK = lockile.good();
                            lockile.close();

                        }  catch  (std::exception & e)  {
                           lock.unlock();
                        }
                        lock.unlock();
                        return isOK;
                    }

                private:
                    std::string _lockfilename;
                    bool _autoclean;
                    bool _created;
                    static Lock mutex;
            };

            Lock LockFile::mutex;

        }


        // backup commands (supported so far)
        enum BackupCommand {
            kBackupStop,  //BACKUP_STOP
            kRestore,  //RESTORE
            kBackup, //BACKUP
            kLoadCrashed //LOAD_CRASHED
        };

        class BackupCommunicator{

            public:

                static void Initialize(std::string host, unsigned int  portIn,unsigned int  portOut,  unsigned int bufferSize,std::size_t timeOut, std::size_t delay ) {

                    BackupCommunicator::host = host;
                    BackupCommunicator::portIn = portIn;
                    BackupCommunicator::portOut = portOut;
                    BackupCommunicator::bufferSize = bufferSize;
                    BackupCommunicator::timeOut = timeOut;
                    BackupCommunicator::delay = delay;
                }

                /*
                 *  makes a request to the backup service
                 *  returns the status of the operation:
                 *       true -- successful
                 *       false -- failed
                 */


                static bool makeRequest(BackupCommand command,bool _dodelay=false) {

                    bool res=false;

                    switch (command) {
                        case kBackupStop:
                            res=BackupCommunicator::sendMessage(std::string("BACKUP_STOP"),_dodelay);
                            break;
                        case kRestore:
                            res = BackupCommunicator::sendMessage(std::string("RESTORE"),_dodelay);
                            break;
                        case kBackup:
                            res= BackupCommunicator::sendMessage(std::string("BACKUP"),_dodelay);
                            break;
                        case kLoadCrashed:
                            res= BackupCommunicator::sendMessage(std::string("LOAD_CRASHED"),_dodelay);
                            break;
                        default:
                            res=false;
                            break;

                    }
                    return res;
                }


                /*
                 *   sends a request to the backup service
                 *   returns the status of the operation:
                 *       true -- successful
                 *       false -- failed
                 */
                static bool sendMessage(std::string message, bool _dodelay=false) {


                    try {
                            {
                                // connect to server of the backup service
                                cxxtools::net::iostream peerIn(BackupCommunicator::host, BackupCommunicator::portIn , BackupCommunicator::bufferSize,BackupCommunicator::timeOut);
                                // send message to the server
                                std::stringstream ss;
                                ss.clear();
                                ss<<message;
                                peerIn << ss.rdbuf() << std::flush;
                                peerIn.close();
                            }
                        if (_dodelay) usleep(BackupCommunicator::delay);
                    }  catch (const std::exception& e) { // if failed to connect to host "localhost" port 9770: errno 111: Connection refused
                        std::cout<<"BackupCommunicator::sendMessage(): failed \n";
                        std::cerr<<"BackupCommunicator::sendMessage(): " << e.what() << std::endl;
                        return false;
                    }


                    return true;
                }

                /*
                 *   get a response  from the backup service
                 *   returns:
                 *       a status from the backup service
                 *       -1 -- if it's failed
                 */
                static int getStatus() {

                    int _res = -1;
                    try {

                        // connect to the port where the response was sent to
                        cxxtools::net::TcpServer server(BackupCommunicator::host, BackupCommunicator::portOut);
                        cxxtools::net::iostream peerOut(server, BackupCommunicator::bufferSize,0,BackupCommunicator::timeOut);
                        std::stringstream ss;
                        ss.clear();
                        // get an answer
                        ss << peerOut.rdbuf();
                        ss >> _res;
                        peerOut.close();
                    }  catch (const std::exception& e) { // if failed to connect to host "localhost" port 9771: errno 111: Connection refused
                        std::cout<<"BackupCommunicator::getStatus(): failed \n";
                        std::cerr<<"BackupCommunicator::getStatus(): " << e.what() << std::endl;
                        return -1;
                    }

                    return _res;

                }



            private:

                static std::string host;
                static unsigned int portIn ;
                static unsigned int portOut ;
                static unsigned int bufferSize ; // in bytes
                static std::size_t timeOut ; // in milliseconds
                static std::size_t delay; // in microseconds



        };

    }

}

std::string  tools::backup::BackupCommunicator::host = "localhost";
unsigned int  tools::backup::BackupCommunicator::portIn =9770;
unsigned int  tools::backup::BackupCommunicator::portOut = 9771;
unsigned int  tools::backup::BackupCommunicator::bufferSize = 8192;
std::size_t  tools::backup::BackupCommunicator::timeOut = 300;
std::size_t  tools::backup::BackupCommunicator::delay = 40000;



/******/

#endif
